#!/bin/sh 

set -e

flask alembic create-or-upgrade

uwsgi -i uwsgi.ini