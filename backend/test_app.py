import sys

from app.application import create_app
from app.database.db_session import engine, create_session
from app.database.modelbase import Base
from app.database.models import User

from alembic.config import Config
from alembic import command
from alembic.runtime.migration import MigrationContext
from sqlalchemy import MetaData

app = create_app()


@app.cli.command()
def create_admin_account():
    with create_session() as session:
        user = session.get(User, 'admin')

        if not user:
            user = User(username='admin',
                        password='some_long_password')
            session.add(user)
            session.commit()


@app.cli.group()
def alembic():
    pass


@alembic.command()
def create_or_upgrade():
    """
    If the current database is not under the alembic revision control, it upgrades the database to the latest revision.
    If not, the database is created from scratch by the SQLAlchemy create_all function and then marked as having the
    latest revision.
    """
    def get_database_revision():
        with engine.connect() as connection:
            context = MigrationContext.configure(connection)
            current_rev = context.get_current_revision()
        return current_rev

    def is_db_empty() -> bool:
        metadata_obj = MetaData()
        metadata_obj.reflect(bind=engine)
        if metadata_obj.tables.get('inventories') is None:
            return True
        else:
            return False

    db_under_revision = get_database_revision()
    alembic_config = Config('alembic.ini')
    if not db_under_revision:
        if is_db_empty():
            # create/update DB
            # check_first do not CREATE tables that already exist
            # logger.debug('running migrations')
            Base.metadata.create_all(engine, checkfirst=True)
            # mark head revision
            # then, load the Alembic configuration and generate the
            # version table, "stamping" it with the most recent rev:
            command.stamp(alembic_config, 'head')
            # logger.debug('schema up-to-date')
        else:
            # logger.error('DB is not empty and not under revision. You should DROP tables first. '
            #              'Only tables, not schema.')
            sys.exit(1)
    else:
        # logger.debug('DB is not under revision and is empty. Creating DB schema.')
        # run migrations
        command.upgrade(alembic_config, 'head')


if __name__ == '__main__':
    app.run()
