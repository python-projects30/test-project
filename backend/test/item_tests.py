import os
import csv
import decimal
import json

import pytest
from sqlalchemy import delete
from werkzeug.datastructures import FileStorage

from app.database.models import User, Item
from app.application import create_app
from app.database.modelbase import Base
from app.database.db_session import engine, create_session

import unittest.mock


@pytest.fixture
def client():
    app = create_app()
    client = app.test_client()
    Base.metadata.create_all(engine)

    yield client


@pytest.fixture
def create_item():
    with create_session() as session:
        data = {
            'internal_identifier': '1',
            'price': '12.34',
            'description': 'test item'
        }
        item = Item(internal_identifier=data['internal_identifier'],
                    price=decimal.Decimal(data['price']),
                    description=data['description'])
        session.add(item)
        session.commit()
        yield data


@pytest.fixture
def delete_items():
    with create_session() as session:
        yield
        statement = delete(Item)
        session.execute(statement)
        session.commit()


def test_ok():
    assert 1 + 2 == 3


def test_fail():
    with pytest.raises(AssertionError):
        assert 1 + 2 == 4


@pytest.mark.usefixtures('delete_items')
def test_create_item(client):
    data = {
        'internal_identifier': '1',
        'price': '12.34',
        'description': 'test item'
    }

    # mock user authorization
    target = 'app.api.tools.__decode_jwt_and_get_user'
    with unittest.mock.patch(target, return_value=User(username='testuser',
                                                       password='123456')):
        response = client.post('/api/item', json=data)
    assert response.status_code == 201
    response_data = json.loads(response.data)
    assert response_data['internal_identifier'] == data['internal_identifier']
    assert response_data['price'] == data['price']
    assert response_data['description'] == data['description']


@pytest.mark.usefixtures('create_item')
@pytest.mark.usefixtures('delete_items')
def test_get_item(client):
    internal_identifier = '1'

    target = 'app.api.tools.__decode_jwt_and_get_user'
    with unittest.mock.patch(target, return_value=User(username='testuser',
                                                       password='123456')):
        response = client.get(f'/api/item/{internal_identifier}')
    assert response.status_code == 200
    response_data = json.loads(response.data)
    assert response_data['internal_identifier'] == '1'
    assert response_data['price'] == '12.34'
    assert response_data['description'] == 'test item'


def test_get_item_not_found(client):
    internal_identifier = '2'

    target = 'app.api.tools.__decode_jwt_and_get_user'
    with unittest.mock.patch(target, return_value=User(username='testuser',
                                                       password='123456')):
        response = client.get(f'/api/item/{internal_identifier}')
    assert response.status_code == 404


@pytest.mark.usefixtures('create_item')
@pytest.mark.usefixtures('delete_items')
def test_update_item(client):
    internal_identifier = '1'
    data = {
        'price': '43.21',
        'description': 'test item but updated'
    }

    # mock user authorization
    target = 'app.api.tools.__decode_jwt_and_get_user'
    with unittest.mock.patch(target, return_value=User(username='testuser',
                                                       password='123456')):
        response = client.patch(f'/api/item/{internal_identifier}',
                                json=data)
    assert response.status_code == 200
    response_data = json.loads(response.data)
    assert response_data['internal_identifier'] == internal_identifier
    assert response_data['price'] == data['price']
    assert response_data['description'] == data['description']


def test_update_item_not_found(client):
    internal_identifier = '4'
    data = {
        'price': '43.21',
        'description': 'test item but updated'
    }

    # mock user authorization
    target = 'app.api.tools.__decode_jwt_and_get_user'
    with unittest.mock.patch(target, return_value=User(username='testuser',
                                                       password='123456')):
        response = client.patch(f'/api/item/{internal_identifier}',
                                json=data)
    assert response.status_code == 404


@pytest.mark.usefixtures('create_item')
@pytest.mark.usefixtures('delete_items')
def test_delete_item(client):
    internal_identifier = '1'
    # mock user authorization
    target = 'app.api.tools.__decode_jwt_and_get_user'
    with unittest.mock.patch(target, return_value=User(username='testuser',
                                                       password='123456')):
        response = client.delete(f'/api/item/{internal_identifier}')
    assert response.status_code == 200


def test_delete_item_not_found(client):
    internal_identifier = '2'
    # mock user authorization
    target = 'app.api.tools.__decode_jwt_and_get_user'
    with unittest.mock.patch(target, return_value=User(username='testuser',
                                                       password='123456')):
        response = client.delete(f'/api/item/{internal_identifier}')
    assert response.status_code == 404


@pytest.mark.usefixtures('delete_items')
def test_import_items(client):
    data = [{'internal_identifier': '1',
             'price': '12.34',
             'description': 'First item'},
            {'internal_identifier': '2',
             'price': '56.78',
             'description': 'Second item'},
            {'internal_identifier': '3',
             'price': '90.00',
             'description': 'Third item'}]

    test_file_name = 'test_import_items.csv'
    with open(test_file_name, 'w', newline='') as test_file:
        writer = csv.writer(test_file)
        for item in data:
            writer.writerow(item.values())

    mock_file = FileStorage(
        stream=open(test_file_name, 'rb'),
        filename=test_file_name,
        content_type='text/csv',
    )
    target = 'app.api.tools.__decode_jwt_and_get_user'
    with unittest.mock.patch(target, return_value=User(username='testuser',
                                                       password='123456')):
        response = client.post(f'/api/import-items',
                               data={'import_items': mock_file},
                               content_type="multipart/form-data")
    assert response.status_code == 200
    returned_items = json.loads(response.data)['items']
    for i, item in enumerate(data):
        for key in item:
            assert returned_items[i][key] == item[key]

    os.remove(test_file_name)
