import csv
import datetime
import decimal
import os

from flask import Blueprint, request, current_app, jsonify, json
from sqlalchemy import select
from sqlalchemy.exc import IntegrityError, NoResultFound, MultipleResultsFound
from sqlalchemy.orm import Session, with_polymorphic, joinedload, selectin_polymorphic
from werkzeug.utils import secure_filename

from app.api.tools import require_authentication, require_session, abort_with_error_response
from app.database.db_session import create_session
from app.database.models import Item, User, Company, Person, Customer, Address, Order

api = Blueprint('api', 'api')


########################################################################################################################
# User                                                                                                                 #
########################################################################################################################
@api.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    try:
        username = data['username']
        password = data['password']
        session = create_session()
        user = User.authenticate(username, password, session)
    except KeyError:
        abort_with_error_response('Error reading user credentials', 400)

    if not user:
        abort_with_error_response('The user does not exist or you provided bad credentials', 400)

    token = user.encode_jwt_token(user.username)

    return {
        'token': token,
        'user': user.as_dict()
    }, 200


########################################################################################################################
# Customer                                                                                                             #
########################################################################################################################
@api.route('/customers', methods=['GET'])
@require_authentication
@require_session
def get_customers(session: Session):
    customers = with_polymorphic(Customer, [Person, Company])
    statement = select(customers)
    customers_selected: list[Company | Person] = session.scalars(statement).all()
    return jsonify([customer.as_dict(relationships={'addresses'}) for customer in customers_selected])


@api.route('/company', methods=['POST'])
@require_authentication
@require_session
def create_company(session: Session):
    """
    Creates company along with its primary (and optionally secondary) address.
    :param session: current DB session
    :return:
    """
    data = request.get_json()
    try:
        new_company = Company(phone_number=data['phone_number'],
                              email_address=data['email_address'],
                              discriminator=Company.DISCRIMINATOR_COMPANY,
                              ico=data['ico'],
                              dic=data['dic'])
    except KeyError:
        abort_with_error_response("Missing info about the company", 400)
    except IntegrityError:
        abort_with_error_response("Missing info about the company", 400)

    try:
        # Primary address is always contact, why would it be primary otherwise?
        new_address = Address(street=data['primary_address']['street'],
                              city=data['primary_address']['city'],
                              postal_code=data['primary_address']['postal_code'],
                              is_primary=True)
    except KeyError:
        abort_with_error_response("At least one full address is mandatory. Provide all the info.", 400)

    new_company.addresses.append(new_address)

    if data.get('secondary_address', None):
        try:
            new_second_address = Address(street=data['secondary_address']['street'],
                                         city=data['secondary_address']['city'],
                                         postal_code=data['secondary_address']['postal_code'],
                                         is_primary=False,
                                         is_contact=data['secondary_address']['is_contact'])
            new_company.addresses.append(new_second_address)
        except KeyError:
            abort_with_error_response("The second address needs all the info. Provide all the info.", 400)
    session.add(new_company)
    session.commit()
    return json.dumps(new_company.as_dict()), 201


@api.route('/company/<string:company_id>', methods=['GET'])
@require_authentication
@require_session
def get_company(company_id: str, session: Session):
    """
    Return company that has the provided company ID
    :param company_id: ID of the company to retrieve
    :param session: current DB session
    """
    company: Company = session.get(Company, company_id)
    if not company:
        abort_with_error_response('Company not found', 404)
    return jsonify(company.as_dict())


@api.route('/company/<string:company_id>', methods=['PATCH'])
@require_authentication
@require_session
def update_company(company_id: str, session: Session):
    """
    Update company that has the provided company ID
    :param company_id: ID of the company to update
    :param session: current DB session
    """
    data = request.get_json()
    company: Company = session.get(Company, company_id)
    if not company:
        abort_with_error_response('Company not found', 404)
    try:
        if data.get('phone_number'):
            company.phone_number = data['phone_number']
        if data.get('email_address'):
            company.email_address = data['email_address']
        if data.get('ico'):
            company.ico = data['ico']
        if data.get('dic'):
            company.dic = data['dic']
        session.commit()
        return jsonify(company.as_dict())
    except IntegrityError:
        abort_with_error_response("The values you provided are not allowed", 400)


@api.route('/company/<string:company_id>', methods=['DELETE'])
@require_authentication
@require_session
def delete_company(company_id: str, session: Session):
    """
    Delete company that has the provided company ID
    :param company_id: ID of the company to delete
    :param session: current DB session
    """
    person: Company = session.get(Company, company_id)
    if not person:
        abort_with_error_response('Company not found', 404)
    session.delete(person)
    session.commit()
    return 'OK', 200


@api.route('/person', methods=['POST'])
@require_authentication
@require_session
def create_person(session: Session):
    """
    Creates person along with their primary (and optionally secondary) address.
    :param session: current DB session
    :return:
    """
    data = request.get_json()
    try:
        new_person = Person(phone_number=data['phone_number'],
                            email_address=data['email_address'],
                            discriminator=Person.DISCRIMINATOR_PERSON,
                            first_name=data['first_name'],
                            last_name=data['last_name'],
                            date_of_birth=datetime.date.fromisoformat(data['date_of_birth']))
    except KeyError:
        abort_with_error_response('Missing info about the company', 400)
    except IntegrityError:
        abort_with_error_response('Missing info about the company', 400)

    try:
        # Primary address is always contact, why would it be primary otherwise?
        new_address = Address(street=data['primary_address']['street'],
                              city=data['primary_address']['city'],
                              postal_code=data['primary_address']['postal_code'],
                              is_primary=True)
    except KeyError:
        abort_with_error_response('At least one full address is mandatory. Provide all the info.', 400)

    new_person.addresses.append(new_address)

    if data.get('secondary_address', None):
        try:
            new_second_address = Address(street=data['secondary_address']['street'],
                                         city=data['secondary_address']['city'],
                                         postal_code=data['secondary_address']['postal_code'],
                                         is_primary=False,
                                         is_contact=data['secondary_address']['is_contact'])
            new_person.addresses.append(new_second_address)
        except KeyError:
            abort_with_error_response('The second address needs all the info. Provide all the info.', 400)
    session.add(new_person)
    session.commit()
    return json.dumps(new_person.as_dict()), 201


@api.route('/person/<string:person_id>', methods=['GET'])
@require_authentication
@require_session
def get_person(person_id: str, session: Session):
    """
    Get person that has the provided person ID
    :param person_id: ID of the person to retrieve
    :param session: current DB session
    """
    person: Person = session.get(Person, person_id)
    if not person:
        abort_with_error_response('Person not found', 404)
    return jsonify(person.as_dict())


@api.route('/person/<string:person_id>', methods=['PATCH'])
@require_authentication
@require_session
def update_person(person_id: str, session: Session):
    """
    Update person that has the provided person ID
    :param person_id: ID of the person to update
    :param session: current DB session
    """
    data = request.get_json()
    person: Person = session.get(Person, person_id)
    if not person:
        abort_with_error_response('Person not found', 404)
    try:
        if data.get('phone_number'):
            person.phone_number = data['phone_number']
        if data.get('email_address'):
            person.email_address = data['email_address']
        if data.get('first_name'):
            person.first_name = data['first_name']
        if data.get('last_name'):
            person.last_name = data['last_name']
        if data.get('date_of_birth'):
            person.date_of_birth = datetime.date.fromisoformat(data['date_of_birth'])
        session.commit()
        return jsonify(person.as_dict())
    except IntegrityError:
        abort_with_error_response("The values you provided are not allowed", 400)


@api.route('/person/<string:person_id>', methods=['DELETE'])
@require_authentication
@require_session
def delete_person(person_id: str, session: Session):
    """
    Delete person that has the provided person ID
    :param person_id: ID of the person to delete
    :param session: current DB session
    """
    person: Person = session.get(Person, person_id)
    if not person:
        abort_with_error_response('Person not found', 404)
    session.delete(person)
    session.commit()
    return 'OK', 200


########################################################################################################################
# Item                                                                                                                 #
########################################################################################################################
@api.route('/item', methods=['POST'])
@require_authentication
@require_session
def create_item(session: Session):
    data = request.get_json()
    try:
        new_item = Item(internal_identifier=data['internal_identifier'],
                        price=decimal.Decimal(data['price']),
                        description=data.get('description', ''))
        session.add(new_item)
        session.commit()
        return json.dumps(new_item.as_dict()), 201
    except IntegrityError:
        abort_with_error_response(f'An item with the provided internal identifier {data["internal_identifier"]} '
                                  f'already exists in the database', 400)
    except KeyError:
        abort_with_error_response('No internal identifier or price tag for the item', 400)
    except decimal.InvalidOperation:
        abort_with_error_response('Wrong price format', 400)


@api.route('/item/<string:item_id>', methods=['GET'])
@require_authentication
@require_session
def get_item(item_id: str, session: Session):
    item: Item = session.get(Item, item_id)
    if item is None:
        abort_with_error_response(f'Item with id {item_id} not found', 404)
    return jsonify(item.as_dict())


@api.route('/item/<string:item_id>', methods=['PATCH'])
@require_authentication
@require_session
def update_item(item_id: str, session: Session):
    data = request.get_json()
    item: Item = session.get(Item, item_id)
    if not item:
        abort_with_error_response(f'Item {item_id} not found', 404)
    price = data.get('price')
    if price is not None:
        item.price = price
    description = data.get('description')
    if description is not None:
        item.description = description
    session.commit()
    return jsonify(item.as_dict())


@api.route('/item/<string:item_id>', methods=['DELETE'])
@require_authentication
@require_session
def delete_item(item_id: str, session: Session):
    item: Item = session.get(Item, item_id)
    if not item:
        abort_with_error_response(f'Item {item_id} not found', 404)
    session.delete(item)
    session.commit()
    return 'OK', 200


@api.route('/items', methods=['GET'])
@require_authentication
@require_session
def get_items(session: Session):
    statement = select(Item)
    items = session.execute(statement).scalars().all()
    return {'items': [item.as_dict() for item in items]}, 200


@api.route('/import-items', methods=['POST'])
@require_authentication
@require_session
def import_items(session: Session, **_kwargs):
    import_items_file_name = 'import_items'
    filename = os.path.join(current_app.config['UPLOAD_FOLDER'],
                            secure_filename(request.files[import_items_file_name].filename))
    request.files[import_items_file_name].save(filename)
    row_number: int | None = None
    items_to_add = []
    try:
        # validation of the file
        with open(filename, 'r', newline='') as file:
            reader = csv.reader(file, strict=True)
            for row_number, row in enumerate(reader):
                if len(row) != 3:
                    abort_with_error_response(f'The file has bad column count ({len(row)}) on line {row_number+1}',
                                              400)
                internal_identifier = row[0]
                if not internal_identifier:
                    abort_with_error_response(f'No internal_identifier on line {row_number+1}', 400)
                price = decimal.Decimal(row[1])
                description = row[2]
                items_to_add.append(Item(internal_identifier=internal_identifier,
                                         price=price,
                                         description=description))
        session.add_all(items_to_add)
        session.commit()
        return {'items': [item.as_dict() for item in items_to_add]}
    except csv.Error:
        abort_with_error_response('The format of the CSV file is incorrect', 400)
    except decimal.InvalidOperation:
        abort_with_error_response(f'Wrong price format on line {row_number+1 if row else None}', 400)
    except IntegrityError:
        abort_with_error_response(f'Some of the items you are trying to import are already in the database', 400)
    finally:
        os.remove(filename)


@api.route('/orders', methods=['GET'])
@require_authentication
@require_session
def get_orders(session: Session):
    """
    Retrieve list of all orders
    :param session: current DB session
    """
    statement = select(Order)
    orders: list[Order] = session.scalars(statement).all()
    return jsonify([order.as_dict(relationships={'items', 'customer'}) for order in orders])


@api.route('/order', methods=['POST'])
@require_authentication
@require_session
def create_orders(session: Session):
    """
    Create order base ond parameters from the request
    :param session: current DB session
    """
    try:
        data = request.get_json()
        order = Order(internal_identifier=data['internal_identifier'])
        item_ids = data['item_ids']
        if len(item_ids) == 0:
            abort_with_error_response('An order has to contain at least one item', 400)
        statement = select(Item).where(Item.internal_identifier.in_(item_ids))
        items: list[Item] = session.scalars(statement).all()
        if len(items) != len(item_ids):
            abort_with_error_response('Some of the items were not found in the database', 400)
        order.items = items
        total_cost = sum([item.price for item in items])
        order.total_cost = total_cost

        customer = session.get(Customer, data['customer_id'])
        if not customer:
            abort_with_error_response('Customer not found', 400)
        order.customer = customer
        session.add(order)
        session.commit()
        return json.dumps(order.as_dict(relationships={'items', 'customer'})), 201
    except KeyError:
        abort_with_error_response('Some information was not provided. Please provide all the info necessary for '
                                  'creating an order', 400)
    except IntegrityError:
        abort_with_error_response('The order internal identifier already exists in the database', 400)


@api.route('/order/<string:order_id>', methods=['GET'])
@require_authentication
@require_session
def get_order(order_id: str, session: Session):
    """
    Retrieve the order with the given ID
    :param order_id: ID of the order to retrieve
    :param session: current DB session
    """
    try:
        statement = select(Order)\
            .where(Order.internal_identifier == order_id)\
            .options(joinedload(Order.items),
                     joinedload(Order.customer).options(selectin_polymorphic(Customer, [Company, Person])))
        order = session.execute(statement).unique().scalars().one()
        return jsonify(order.as_dict(relationships={'items', 'customer'}))
    except NoResultFound:
        abort_with_error_response("Order not found", 404)
    except MultipleResultsFound:
        abort_with_error_response("Wrong query", 400)


@api.route('/order/<string:order_id>', methods=['PATCH'])
@require_authentication
@require_session
def update_order(order_id: str, session: Session):
    """
    Update the order with the given ID
    :param order_id: ID of the order to update
    :param session: current DB session
    """
    data = request.get_json()
    order: Order = session.get(Order, order_id)
    if not order:
        abort_with_error_response("Order not found", 404)

    if data.get('item_ids'):
        item_ids = data['item_ids']
        if len(item_ids) == 0:
            abort_with_error_response('An order has to contain at least one item', 400)
        statement = select(Item).where(Item.internal_identifier.in_(item_ids))
        items: list[Item] = session.scalars(statement).all()
        if len(items) != len(item_ids):
            abort_with_error_response('Some of the items were not found in the database', 400)
        order.items = items
        total_cost = sum([item.price for item in items])
        order.total_cost = total_cost

    if data.get('customer_id'):
        customer = session.get(Customer, data['customer_id'])
        if not customer:
            abort_with_error_response('Customer not found', 400)
        order.customer = customer

    session.commit()
    return jsonify(order.as_dict(relationships={'items', 'customer'}))


@api.route('/order/<string:order_id>', methods=['DELETE'])
@require_authentication
@require_session
def delete_order(order_id: str, session: Session):
    """
    Delete the order with the given ID
    :param order_id: ID of the order to delete
    :param session: current DB session
    """
    order = session.get(Order, order_id)
    if not order:
        abort_with_error_response("Order not found", 404)
    session.delete(order)
    session.commit()
    return 'OK', 200


@api.route('/address/<int:address_id>', methods=['GET'])
@require_authentication
@require_session
def get_address(address_id: int, session: Session):
    """
    Retrieve the address with the given ID
    :param address_id: ID of the address to retrieve
    :param session: current DB session
    """
    address: Address = session.get(Address, address_id)
    if not address:
        abort_with_error_response("Address not found", 404)
    return jsonify(address.as_dict())


@api.route('/address', methods=['POST'])
@require_authentication
@require_session
def create_address(session: Session):
    try:
        data = request.get_json()
        customer: Customer = session.get(Customer, data['customer'])
        if not customer:
            abort_with_error_response("The customer was not found", 400)
        if len(customer.addresses) == 2:
            abort_with_error_response("The customer already has two addresses", 405)
        address = Address(street=data['street'],
                          city=data['city'],
                          postal_code=data['postal_code'],
                          is_primary=False,  # primary address cannot be added as it is always the first one
                          is_contact=data['is_contact'])
        address.customer = customer
        session.add(address)
        session.commit()
        return jsonify(address.as_dict())
    except KeyError:
        abort_with_error_response("Parameters missing", 400)


@api.route('/address/<int:address_id>', methods=['PATCH'])
@require_authentication
@require_session
def update_address(address_id: int, session: Session):
    """
    Update the address with the given ID
    :param address_id: ID of the address to update
    :param session: current DB session
    """
    data = request.get_json()
    address: Address = session.get(Address, address_id)
    if not address:
        abort_with_error_response("Address not found", 404)
    if data.get('street'):
        address.street = data['street']
    if data.get('city'):
        address.city = data['city']
    if data.get('postal_code'):
        address.postal_code = data['postal_code']
    if (is_contact := data.get('is_contact')) is not None:
        if not is_contact \
                and address.is_contact \
                and len([addr for addr in address.customer.addresses if addr.is_contact]) == 1:
            abort_with_error_response("The customer needs at least one contact address", 400)
        address.is_contact = is_contact
    session.commit()
    return jsonify(address.as_dict())


@api.route('/address/<int:address_id>', methods=['DELETE'])
@require_authentication
@require_session
def delete_address(address_id: int, session: Session):
    """
    Delete the address with the given ID
    :param address_id: ID of the address to delete
    :param session: current DB session
    """
    address: Address = session.get(Address, address_id)
    if not address:
        abort_with_error_response("Address not found", 404)
    if address.is_primary:
        abort_with_error_response("Primary address cannot be deleted", 400)
    if address.is_contact and len([addr for addr in address.customer.addresses if addr.is_contact]) == 1:
        abort_with_error_response("The customer needs at least one contact address", 400)
    if len(address.customer.addresses) == 1:
        abort_with_error_response("A customer needs to have at least one address", 400)
    session.delete(address)
    session.commit()
    return 'OK', 200
