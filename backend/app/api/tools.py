from functools import wraps
from typing import Final

import jwt
from flask import request, current_app, Response, json, abort
from sqlalchemy import select
from sqlalchemy.exc import SQLAlchemyError, NoResultFound, MultipleResultsFound

from app.database.db_session import create_session
from app.database.models import User


ERROR_CODES: Final[dict[int, str]] = {
    400: 'api.bad_request_error',
    401: 'api.authorization_error',
    404: 'api.not_found_error',
    500: 'api.internal_server_error'
}


def abort_with_error_response(message, status_code: int = 500, error_code: str = None):
    """
    Create an error response and call abort function
    :param message:
    :param status_code:
    :param error_code:
    :return:
    """
    if error_code is None:
        error_code = ERROR_CODES[status_code]
    abort(Response(response=json.dumps({'message': message,
                                        'error_code': error_code,
                                        'status_code': status_code}),
                   status=status_code,
                   mimetype='application/json'))


def __decode_jwt_and_get_user(session):
    authentication_headers = request.headers.get('authentication', '').split()
    if len(authentication_headers) != 2:
        abort_with_error_response('Invalid token.', 401)
    token = authentication_headers[1]
    data = jwt.decode(token, current_app.config['SECRET_KEY'], algorithms=["HS256"])
    statement = select(User).where(User.username == data['sub'])
    user = session.execute(statement).scalar_one()
    return user


def __authenticate_user() -> User:
    try:
        session = create_session()
        user = __decode_jwt_and_get_user(session)
        return user
    except jwt.ExpiredSignatureError:
        abort_with_error_response('Token expired', 401)
    except jwt.InvalidTokenError:
        abort_with_error_response('Invalid token', 401)
    except SQLAlchemyError:
        abort_with_error_response('The server has encountered an unexpected problem', 500)
    except (NoResultFound, MultipleResultsFound):
        abort_with_error_response('Unknown user', 401)


def require_authentication(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        __authenticate_user() if require_authentication else None
        return f(*args, **kwargs)
    return wrapper


def require_session(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        with create_session() as session:
            return f(*args, **kwargs, session=session)
    return wrapper
