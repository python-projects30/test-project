import os.path

from flask import Flask, Blueprint
from flask_cors import CORS

from app.config import config


def create_app(name: str = 'TEST_APP') -> Flask:
    app = Flask(__name__)
    # apply config read from configuration file
    app.config.from_object(_ServerConfig())

    # use CORS for cross-domain api access
    CORS(app, resources={r"/api/*": {"origins": "*"}})

    from app.api.methods import api
    app.register_blueprint(api, url_prefix='/api')

    # static frontend files from production directory (in debugging, frontend is to be served by Quasar without Flask)
    root = Blueprint('root', 'root', static_folder='../frontend/dist/spa/', static_url_path='/')

    @root.route('/')
    def catch_all():
        return root.send_static_file('index.html')

    app.register_blueprint(root, url_prefix="/")

    return app


class _ServerConfig:
    """
    A Flask config class with init based on config file.
    """

    def __init__(self):
        self.DEBUG = config.get('server.debug')
        self.TESTING = config.get('server.testing')
        self.SECRET_KEY = config.get('server.secret_key')
        upload_folder = os.path.abspath(config.get('server.upload_dir'))
        self.UPLOAD_FOLDER = upload_folder

        # json optimization - no sorting, no pretty print - brings performance increase
        self.JSONIFY_PRETTYPRINT_REGULAR = False
