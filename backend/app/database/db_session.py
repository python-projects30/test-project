from app.config import config
import sqlalchemy.orm
# noinspection PyUnresolvedReferences
import app.database.models  # is unused but necessary so SQLAlchemy see the models

factory: sqlalchemy.orm.sessionmaker | None = None
engine: sqlalchemy.engine.Engine | None = None

if not factory:
    user = config.get('database.user')
    password = config.get('database.password')
    url = config.get('database.url')
    db_name = config.get('database.name')
    port = config.get('database.port')

    database_uri = f'postgresql+psycopg2://{user}:{password}@{url}:{port}/{db_name}'

    engine_options = {
        'echo': config.get('server.debug') and config.get('debug.engine_echo_enabled'),
        'future': True  # Core of SQLAlchemy 2.0
    }

    engine = sqlalchemy.create_engine(database_uri, **engine_options)
    factory = sqlalchemy.orm.sessionmaker(autocommit=False, autoflush=False, bind=engine)

    # We use alembic for this, so we don't need this.
    # Base.metadata.create_all(engine)

    # Enable if running multiple processes, e.g., app.run(processes=2) etc. See links below for more info:
    # https://virtualandy.wordpress.com/2019/09/04/a-fix-for-operationalerror-psycopg2-operationalerror-ssl-error-decryption-failed-or-bad-record-mac/
    # https://docs.sqlalchemy.org/en/14/core/connections.html#engine-disposal
    # engine.dispose()


def create_session(**kwargs) -> sqlalchemy.orm.Session:
    """
    Create session from factory that can be used as a context manager
    :param kwargs:
    :return:
    """
    global factory
    return factory(**kwargs)
