import decimal
import os
import hashlib
import jwt
import uuid

from sqlalchemy.orm import Session, relationship, column_property

from app.database.modelbase import Base
from typing import Final, Union
from sqlalchemy import Column, String, Integer, Boolean, Numeric, ForeignKey, case, Date, Table

import datetime

from app.config import config


class PasswordEmptyError(Exception):
    pass


class BaseModel(Base):
    """
    Base class all models should use.
    """
    __abstract__ = True

    __do_not_serialize__: set[str] = set()

    def as_dict(self, relationships: set[str] = None):
        def __add_attribute_to_dict(obj, selected_dict, col_name, relationships: set[str] = None):
            if col.name not in self.__do_not_serialize__:
                attribute = getattr(obj, col_name)
                # Decimal values are not serializable by default, this makes them serializable as strings
                if isinstance(attribute, decimal.Decimal):
                    attribute = str(attribute)
                elif isinstance(attribute, datetime.date):
                    attribute = attribute.isoformat()
                elif isinstance(attribute, list):
                    attribute = [item.as_dict(relationships=relationships) for item in attribute]
                elif isinstance(attribute, BaseModel):
                    attribute = attribute.as_dict(relationships=relationships)
                selected_dict[col_name] = attribute
        dict_to_return = dict()
        for col in self.__table__.columns:
            __add_attribute_to_dict(self, dict_to_return, col.name, relationships=relationships)
        if relationships:
            for rship in self.__mapper__.relationships:
                if rship.key in relationships:
                    __add_attribute_to_dict(self, dict_to_return, rship.key, relationships=relationships)
        if len(self._sa_class_manager._bases) != 0:
            for base in self._sa_class_manager._bases:
                for col in base.class_.__table__.columns:
                    __add_attribute_to_dict(self, dict_to_return, col.name, relationships=relationships)
                if relationships:
                    for rship in base.class_.__mapper__.relationships:
                        if rship.key in relationships:
                            __add_attribute_to_dict(self, dict_to_return, rship.key, relationships=relationships)

        return dict_to_return

    @staticmethod
    def generate_uuid() -> str:
        return uuid.uuid4().hex


class User(BaseModel):
    """
    Represents a user that can log into the system
    """
    __tablename__: Final[str] = 'users'

    __PASSWORD_SALT_LENGTH: Final[int] = 64
    __BASE_ITERATION_NUMBER: Final[int] = 300000
    __HASH_ALGORITHM: Final[str] = 'sha256'

    __do_not_serialize__ = {'password_hash', }

    username = Column(String, primary_key=True, index=True)
    password_hash = Column(String)

    def __init__(self, username: str, password: str | None):
        self.username = username
        self.__set_password(password)

    def __hash_password(self, password: str, salt: str, iterations: int) -> str:
        return None if password is None else hashlib.pbkdf2_hmac(self.__HASH_ALGORITHM,
                                                                 password.encode('utf-8'),
                                                                 bytes.fromhex(salt),
                                                                 iterations=iterations).hex()

    def __set_password(self, new_password: str) -> None:
        password_salt = os.urandom(self.__PASSWORD_SALT_LENGTH).hex()
        iterations = self.__BASE_ITERATION_NUMBER
        password_hash = self.__hash_password(new_password, password_salt, iterations)
        if password_hash is None:
            raise PasswordEmptyError('User password cannot be set empty!')
        self.password_hash = f'{password_hash}:{password_salt}'

    @classmethod
    def authenticate(cls, username: str, password: str, session: Session) -> Union['User', None]:
        if not password:
            return None

        user = session.get(cls, username)

        if not user or not user.password_hash:
            return None

        password_hash, password_salt = user.password_hash.split(':')

        if password_hash != user.__hash_password(password, password_salt, cls.__BASE_ITERATION_NUMBER):
            return None

        return user

    @staticmethod
    def encode_jwt_token(username):
        """
        Generate the authentication token
        :param username: username of the user for whom to create the authentication token
        """
        utc_time_now = datetime.datetime.utcnow()
        payload = {
            'exp': utc_time_now + datetime.timedelta(minutes=config.get('server.max_session_lifetime')),
            'iat': utc_time_now,
            'sub': username,
        }
        return jwt.encode(
            payload,
            config.get('server.secret_key'),
            algorithm='HS256'
        )


class Customer(BaseModel):
    """
    Base class for companies and persons
    """
    __tablename__ = 'customers'

    DISCRIMINATOR_COMPANY = 'company'
    DISCRIMINATOR_PERSON = 'person'

    id = Column(Integer, primary_key=True, autoincrement=True)
    phone_number = Column(String, nullable=False)
    email_address = Column(String, unique=True, nullable=False)
    discriminator = Column(String)
    customer_type = column_property(
        case([
            (discriminator == DISCRIMINATOR_COMPANY, 'companies'),
            (discriminator == DISCRIMINATOR_PERSON, 'persons'),
        ], else_='customers')
    )

    addresses = relationship('Address', back_populates='customer', cascade='all, delete-orphan', passive_deletes=True)

    orders = relationship('Order', back_populates='customer', passive_deletes=True)

    __mapper_args__ = {
        'polymorphic_identity': 'customers',
        'polymorphic_on': customer_type,
    }


class Company(Customer):
    """
    Represents a company that can have orders
    """
    __tablename__ = 'companies'

    id = Column(Integer, ForeignKey('customers.id'), primary_key=True)
    ico = Column(Integer)
    dic = Column(String)

    __mapper_args__ = {
        'polymorphic_identity': 'companies',
    }


class Person(Customer):
    """
    Represents a person that can have orders
    """
    __tablename__ = 'persons'

    id = Column(Integer, ForeignKey('customers.id'), primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    date_of_birth = Column(Date)

    __mapper_args__ = {
        'polymorphic_identity': 'persons',
    }


class Address(BaseModel):
    """
    Each customer can have two addresses - primary and secondary. One should be contact.
    """
    __tablename__ = 'addresses'

    id = Column(Integer, primary_key=True, autoincrement=True)
    street = Column(String, nullable=False)
    city = Column(String, nullable=False)
    postal_code = Column(String, nullable=False)
    is_primary = Column(Boolean, nullable=False, default=False)
    is_contact = Column(Boolean, nullable=False, default=True)

    customer_id = Column(Integer, ForeignKey('customers.id', onupdate='cascade', ondelete='cascade'))
    customer = relationship('Customer', back_populates='addresses')


order_item_association_table = Table(
    'order_item_association_table',
    Base.metadata,
    Column('orders_id', ForeignKey('orders.internal_identifier'), primary_key=True),
    Column('items_id', ForeignKey('items.internal_identifier'), primary_key=True),
)


class Order(BaseModel):
    """
    Represents an order. An order should have at least one item in it.
    """
    __tablename__ = 'orders'

    internal_identifier = Column(String, primary_key=True, index=True)
    total_cost = Column(Numeric(precision=12, scale=2))

    customer_id = Column(Integer, ForeignKey('customers.id', onupdate='cascade', ondelete='cascade'))
    customer = relationship('Customer', back_populates='orders')

    items = relationship('Item', secondary=order_item_association_table, back_populates='orders')


class Item(BaseModel):
    """
    Represents an item.
    """
    __tablename__ = 'items'

    internal_identifier = Column(String, primary_key=True, index=True)
    price = Column(Numeric(precision=12, scale=2), nullable=False)
    description = Column(String, default='')

    orders = relationship('Order', secondary=order_item_association_table, back_populates='items')
