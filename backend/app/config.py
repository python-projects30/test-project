import os
import qtoml

DEFAULT_CONFIG_PATH = 'config/production.toml'

try:
    config_path = os.environ['TEST_PROJECT_CONFIG_PATH']
except KeyError:
    config_path = DEFAULT_CONFIG_PATH


class TOMLConfig:
    data = {}

    def __init__(self, filepath: str):
        with open(filepath, 'r') as file:
            self.data = qtoml.load(file)

    def get(self, key: str):
        list_of_keys = key.split('.')
        value = self.data
        for single_key in list_of_keys:
            value = value[single_key]
        return value

    def __repr__(self):
        return str(self.data)


config = TOMLConfig(config_path)
