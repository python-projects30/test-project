
const routes = [
  {
    name: 'Homepage',
    path: '/',
    component: () => import('pages/home/HomePage'),
  },

  {
    name: 'Login',
    path: '/login',
    component: () => import('pages/login/LoginPage'),
  },

  {
    name: 'Item',
    path: '/item/:item_id',
    props: (route) => ({ itemID: route.params.item_id }),
    component: () => import('pages/items/ItemPage'),
  },

  {
    name: 'Order',
    path: '/order/:order_id?',
    props: (route) => ({ order_id: route.params.order_id ? route.params.order_id : null }),
    component: () => import('pages/orders/OrderPage'),
  },

  {
    name: 'Orders',
    path: '/orders',
    component: () => import('pages/orders/OrdersPage'),
  },

  {
    name: 'Items',
    path: '/items',
    component: () => import('pages/items/ItemsPage'),
  },

  {
    name: 'Customers',
    path: '/customers',
    component: () => import('pages/customers/CustomersPage'),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
