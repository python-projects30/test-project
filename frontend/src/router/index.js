import { route } from 'quasar/wrappers'
import { createRouter, createMemoryHistory, createWebHistory, createWebHashHistory } from 'vue-router'
import routes from './routes'
import {useStore} from 'stores/store'

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function ({ /* store, ssrContext */ }) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : (process.env.VUE_ROUTER_MODE === 'history' ? createWebHistory : createWebHashHistory)


  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.MODE === 'ssr' ? void 0 : process.env.VUE_ROUTER_BASE)
  })

  Router.beforeEach(async (to) => {
    const store = useStore()

    if (to.name === 'Login') {
      // Always log out the user when going to the login page
      if (store) {
        store.logout()
      }
      return true
    } else {
      if (!store.isUserAuthenticated || !store.user) {
        // Always log out the user when going to the login page
        if (store) {
          store.logout()
        }
        // If authentication is necessary but the user is not authenticated or no user is in the store variable,
        // go to the login page but remember the page the user is trying to access.
        return { name: 'Login' }
      }
    }


  })

  return Router
})
