import { defineStore } from 'pinia'
import { LocalStorage } from 'quasar'
import { api } from 'boot/axios'

export const useStore = defineStore('store', {
  state: () => ({
    access_token: LocalStorage.getItem('access_token'),
    user: LocalStorage.getItem('user'),
  }),
  getters: {
    isUserAuthenticated: (state) => {
      return isJWTValid(state.access_token)
    }
  },
  actions: {
    async login(username, password) {
      const response = await callApi(this, 'post', 'login', { data: {
        username,
        password,
      }})
      setValue(this, 'user', response.data.user, true)
      setValue(this, 'access_token', response.data.token, true)
    },

    logout() {
      LocalStorage.remove('access_token')
      LocalStorage.remove('user')
      this.access_token = null
      this.user = null
    },

    getCustomers() {
      return callApi(this, 'get', 'customers')
    },

    createCompany(data) {
      return callApi(this, 'post', 'company', { data })
    },

    updateCompany(id, data) {
      return callApi(this, 'patch', `company/${id}`, { data })
    },

    deleteCompany(id) {
      return callApi(this, 'delete', `company/${id}`)
    },

    createPerson(data) {
      return callApi(this, 'post', 'person', { data })
    },

    updatePerson(id, data) {
      return callApi(this, 'patch', `person/${id}`, { data })
    },

    deletePerson(id) {
      return callApi(this, 'delete', `person/${id}`)
    },

    createItem(internal_identifier, price, description='') {
      return callApi(this, 'post', `item`, { data: {
          internal_identifier,
          price,
          description,
        }
      })
    },

    getItem(item_id) {
      return callApi(this, 'get', `item/${item_id}`)
    },

    updateItem(item_id, price, description) {
      return callApi(this, 'patch', `item/${item_id}`, {data: {
          price,
          description,
        }
      })
    },

    deleteItem(item_id) {
      return callApi(this, 'delete', `item/${item_id}`)
    },

    importItems(file) {
      const files = {
        import_items: file
      }
      return callApi(this, 'post', 'import-items', { files })
    },

    getItems() {
      return callApi(this, 'get', 'items')
    },

    getOrders() {
      return callApi(this, 'get', 'orders')
    },

    getOrder(order_id) {
      return callApi(this, 'get', `order/${order_id}`)
    },

    createOrder(internal_identifier, item_ids, customer_id) {
      return callApi(this, 'post', `order`, {
        data: {
          internal_identifier,
          item_ids,
          customer_id,
        }
      })
    },

    updateOrder(internal_identifier, item_ids, customer_id) {
      return callApi(this, 'patch', `order/${internal_identifier}`, {
        data: {
          internal_identifier,
          item_ids,
          customer_id,
        }
      })
    },

    deleteOrder(order_id) {
      return callApi(this, 'delete', `order/${order_id}`)
    },

    createAddress(data) {
      return callApi(this, 'post', `address`, { data })
    },
    updateAddress(address_id, data) {
      return callApi(this, 'patch', `address/${address_id}`, { data })
    },
    deleteAddress(address_id) {
      return callApi(this, 'delete', `address/${address_id}`)
    }
  },
})

function setValue(context, key, value, to_storage = false) {
  context[key] = value
  if (to_storage) {
    localStorage.setItem(key, value)
  }
}

function isJWTValid(token) {
  if (!token || token.split('.').length < 3) {
    return false
  }
  // window.atob used because atob alone caused a strange deprecation warning
  const data = JSON.parse(window.atob(token.split('.')[1]))
  const exp = new Date(data.exp * 1000) // JS deals with dates in milliseconds since epoch, Python in seconds
  const now = new Date()
  return now < exp
}

function callApi(context, method, url,
                 { params = null, data = null, authentication_headers = true, files = null }
                   = { params: null, data: null, authentication_headers: true, files: null }) {

  let headers = {}

  let data_to_send
  if (files) {
    let got_files = false
    data_to_send = new FormData()
    for (let id in files) {
      if (Object.prototype.hasOwnProperty.call(files, id)) {
        data_to_send.append(id, files[id])
        got_files = true
      }
    }
    if (got_files) {
      headers['Content-Type'] = 'multipart/form-data'
    }
    for (let key in data) {
      data_to_send.append(key, data[key])
    }

  } else {
    data_to_send = data
  }

  if (authentication_headers) {
    headers.authentication = `Bearer: ${context.access_token}`
  }

  return api({
    method,
    url,
    params,
    data: data_to_send,
    headers,
  })
}
