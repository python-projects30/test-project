/**
 * Formats address in the format 'street, postal_code city'
 * @param address the object containing the address info
 * @returns { string }
 */
export function formatAddress(address) {
  return `${address.street}, ${address.postal_code} ${address.city}, ${address.is_contact ? "contact" : "not contact"}`
}
