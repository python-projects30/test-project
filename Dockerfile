FROM python:3.10-slim-bullseye

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV FLASK_APP='test_app.py'
ENV TEST_PROJECT_CONFIG_PATH=config/production.toml
EXPOSE 5000

# Set working directory to /test_project/backend
WORKDIR /test_project/backend

# Copy backend, install pipenv and install all necessary packages
COPY ./backend ./

# Install dependencies (for building packages when installing Python dependencies)
# Install pipenv via pip
# Install Python dependencies for Plus
# Remove some dependencies that are not necessary (gcc)
RUN apt-get update \
    && apt-get install -y  --no-install-recommends gcc libpq-dev python-dev \
    && pip install --upgrade pip \
    && pip install pipenv \
    && pipenv install --system --deploy \
    && apt-get remove -y gcc python-dev \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && pip uninstall pipenv -y

# Copy the rest of necessary files
# Copies only the final frontend build
COPY ./frontend/dist ./../frontend/dist

# Adds file with the current version
ADD VERSION ../

# Set entrypoint
#CMD ["uwsgi", "--ini", "uwsgi.ini"]
CMD ["./docker-entrypoint.sh"]